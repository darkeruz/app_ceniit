package apps.ceniit.appceniit;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


import apps.ceniit.appceniit.R;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {


    private Button _facebook_bt;
    String _urlF = "https://www.facebook.com/CeniitUNLaR/";

    private AppBarConfiguration mAppBarConfiguration;
    private View v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(

                // AQUI AGREGAN EL R ID DE CADA FRAME
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,R.id.mapsFragment,R.id.proyectos2,R.id.capacitaciones,R.id.redesSocialesFragment)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

    }



    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    //BOTONES REDES SOCIALES

    public void irFacebook (View view){

        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/CeniitUNLaR/"));
        startActivity(i);

    }

    public void irTwitter (View view){

        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/CeniitU"));
        startActivity(i);

    }


    public void irInstagram (View view){

        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/ceniitunlar/"));
        startActivity(i);

    }

    public void masInformacion (View view){

        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/file/d/1APfr5ke7XH7I_gtpselTVn0E9CsbhXmX/view?usp=drivesdk"));
        startActivity(i);

    }


}